﻿namespace TPCSP_Lab_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_ValueSet = new System.Windows.Forms.ListBox();
            this.button_GetRandomValueSet = new System.Windows.Forms.Button();
            this.button_SortByDigits = new System.Windows.Forms.Button();
            this.numericUpDown_randomValuesCap = new System.Windows.Forms.NumericUpDown();
            this.listBox_SortedList = new System.Windows.Forms.ListBox();
            this.button_AddToValueSet = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_ClearOriginalList = new System.Windows.Forms.Button();
            this.numericUpDown_ByHandValues = new System.Windows.Forms.NumericUpDown();
            this.button_SortedListClear = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button_ClearAll = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_randomValuesCap)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ByHandValues)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox_ValueSet
            // 
            this.listBox_ValueSet.FormattingEnabled = true;
            this.listBox_ValueSet.ItemHeight = 16;
            this.listBox_ValueSet.Location = new System.Drawing.Point(16, 15);
            this.listBox_ValueSet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listBox_ValueSet.Name = "listBox_ValueSet";
            this.listBox_ValueSet.Size = new System.Drawing.Size(371, 516);
            this.listBox_ValueSet.TabIndex = 0;
            // 
            // button_GetRandomValueSet
            // 
            this.button_GetRandomValueSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_GetRandomValueSet.Location = new System.Drawing.Point(8, 62);
            this.button_GetRandomValueSet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_GetRandomValueSet.Name = "button_GetRandomValueSet";
            this.button_GetRandomValueSet.Size = new System.Drawing.Size(272, 54);
            this.button_GetRandomValueSet.TabIndex = 1;
            this.button_GetRandomValueSet.Text = "Get random value set";
            this.button_GetRandomValueSet.UseVisualStyleBackColor = true;
            this.button_GetRandomValueSet.Click += new System.EventHandler(this.Button_GetRandomValueSet_Click);
            // 
            // button_SortByDigits
            // 
            this.button_SortByDigits.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button_SortByDigits.Location = new System.Drawing.Point(11, 23);
            this.button_SortByDigits.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_SortByDigits.Name = "button_SortByDigits";
            this.button_SortByDigits.Size = new System.Drawing.Size(272, 49);
            this.button_SortByDigits.TabIndex = 2;
            this.button_SortByDigits.Text = "Sort by digits";
            this.button_SortByDigits.UseVisualStyleBackColor = true;
            this.button_SortByDigits.Click += new System.EventHandler(this.Button_SortByDigits_Click);
            // 
            // numericUpDown_randomValuesCap
            // 
            this.numericUpDown_randomValuesCap.Location = new System.Drawing.Point(8, 23);
            this.numericUpDown_randomValuesCap.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numericUpDown_randomValuesCap.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_randomValuesCap.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown_randomValuesCap.Name = "numericUpDown_randomValuesCap";
            this.numericUpDown_randomValuesCap.Size = new System.Drawing.Size(272, 22);
            this.numericUpDown_randomValuesCap.TabIndex = 3;
            this.numericUpDown_randomValuesCap.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // listBox_SortedList
            // 
            this.listBox_SortedList.FormattingEnabled = true;
            this.listBox_SortedList.ItemHeight = 16;
            this.listBox_SortedList.Location = new System.Drawing.Point(396, 15);
            this.listBox_SortedList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listBox_SortedList.Name = "listBox_SortedList";
            this.listBox_SortedList.Size = new System.Drawing.Size(373, 516);
            this.listBox_SortedList.TabIndex = 4;
            // 
            // button_AddToValueSet
            // 
            this.button_AddToValueSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button_AddToValueSet.Location = new System.Drawing.Point(8, 55);
            this.button_AddToValueSet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_AddToValueSet.Name = "button_AddToValueSet";
            this.button_AddToValueSet.Size = new System.Drawing.Size(272, 47);
            this.button_AddToValueSet.TabIndex = 6;
            this.button_AddToValueSet.Text = "Add";
            this.button_AddToValueSet.UseVisualStyleBackColor = true;
            this.button_AddToValueSet.Click += new System.EventHandler(this.Button_AddToValueSet_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDown_randomValuesCap);
            this.groupBox1.Controls.Add(this.button_GetRandomValueSet);
            this.groupBox1.Location = new System.Drawing.Point(784, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(291, 127);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Random";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_ClearOriginalList);
            this.groupBox2.Controls.Add(this.numericUpDown_ByHandValues);
            this.groupBox2.Controls.Add(this.button_AddToValueSet);
            this.groupBox2.Location = new System.Drawing.Point(784, 150);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(291, 165);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "By hand";
            // 
            // button_ClearOriginalList
            // 
            this.button_ClearOriginalList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button_ClearOriginalList.Location = new System.Drawing.Point(11, 110);
            this.button_ClearOriginalList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_ClearOriginalList.Name = "button_ClearOriginalList";
            this.button_ClearOriginalList.Size = new System.Drawing.Size(272, 47);
            this.button_ClearOriginalList.TabIndex = 7;
            this.button_ClearOriginalList.Text = "Clear";
            this.button_ClearOriginalList.UseVisualStyleBackColor = true;
            this.button_ClearOriginalList.Click += new System.EventHandler(this.Button_ClearOriginalList_Click);
            // 
            // numericUpDown_ByHandValues
            // 
            this.numericUpDown_ByHandValues.Location = new System.Drawing.Point(11, 23);
            this.numericUpDown_ByHandValues.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numericUpDown_ByHandValues.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ByHandValues.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown_ByHandValues.Name = "numericUpDown_ByHandValues";
            this.numericUpDown_ByHandValues.Size = new System.Drawing.Size(272, 22);
            this.numericUpDown_ByHandValues.TabIndex = 4;
            this.numericUpDown_ByHandValues.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown_ByHandValues.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericUpDown_ByHandValues_KeyPress);
            // 
            // button_SortedListClear
            // 
            this.button_SortedListClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button_SortedListClear.Location = new System.Drawing.Point(11, 80);
            this.button_SortedListClear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_SortedListClear.Name = "button_SortedListClear";
            this.button_SortedListClear.Size = new System.Drawing.Size(272, 47);
            this.button_SortedListClear.TabIndex = 9;
            this.button_SortedListClear.Text = "Clear";
            this.button_SortedListClear.UseVisualStyleBackColor = true;
            this.button_SortedListClear.Click += new System.EventHandler(this.Button_SortedListClear_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button_ClearAll);
            this.groupBox3.Controls.Add(this.button_SortByDigits);
            this.groupBox3.Controls.Add(this.button_SortedListClear);
            this.groupBox3.Location = new System.Drawing.Point(784, 322);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(291, 209);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Sort";
            // 
            // button_ClearAll
            // 
            this.button_ClearAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button_ClearAll.Location = new System.Drawing.Point(11, 134);
            this.button_ClearAll.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_ClearAll.Name = "button_ClearAll";
            this.button_ClearAll.Size = new System.Drawing.Size(272, 47);
            this.button_ClearAll.TabIndex = 10;
            this.button_ClearAll.Text = "Clear All";
            this.button_ClearAll.UseVisualStyleBackColor = true;
            this.button_ClearAll.Click += new System.EventHandler(this.Button_ClearAll_Click);
            // 
            // Form1
            // 
            this.AcceptButton = this.button_AddToValueSet;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 554);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listBox_SortedList);
            this.Controls.Add(this.listBox_ValueSet);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "TPCSP Lab 1";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_randomValuesCap)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ByHandValues)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_ValueSet;
        private System.Windows.Forms.Button button_GetRandomValueSet;
        private System.Windows.Forms.Button button_SortByDigits;
        private System.Windows.Forms.NumericUpDown numericUpDown_randomValuesCap;
        private System.Windows.Forms.ListBox listBox_SortedList;
        private System.Windows.Forms.Button button_AddToValueSet;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown_ByHandValues;
        private System.Windows.Forms.Button button_ClearOriginalList;
        private System.Windows.Forms.Button button_SortedListClear;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button_ClearAll;
    }
}

