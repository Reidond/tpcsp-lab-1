﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPCSP_Lab_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button_GetRandomValueSet_Click(object sender, EventArgs e)
        {
            GetRandomValueList(listBox_ValueSet, (int) numericUpDown_randomValuesCap.Value);
        }

        private void Button_SortByDigits_Click(object sender, EventArgs e)
        {
            SortByDigit(listBox_ValueSet, listBox_SortedList);
        }

        private void GetRandomValueList(ListBox listBox, int listSize)
        {
            listBox.Items.Clear();
            var rnd = new Random();
            var randomList = Enumerable.Range(1, listSize).OrderBy(r => rnd.Next()).ToList();
            foreach (var randomNum in randomList)
            {
                listBox.Items.Add(randomNum);
            }
        }

        private void SortByDigit(ListBox listBoxUnsorted, ListBox listBoxSorted)
        {
            var list = listBoxUnsorted.Items.Cast<int>().OrderBy(item => item.ToString().Sum(c => c - '0')).ToList();
            listBoxSorted.Items.Clear();
            listBoxSorted.Items.AddRange(list.Cast<object>().ToArray());
            //foreach (var listItem in list)
            //{
            //    listBoxSorted.Items.Add(listItem);
            //}
        }

        private void Button_AddToValueSet_Click(object sender, EventArgs e)
        {
            listBox_ValueSet.Items.Add((int) numericUpDown_ByHandValues.Value);
        }

        private void Button_ClearOriginalList_Click(object sender, EventArgs e)
        {
            listBox_ValueSet.Items.Clear();
        }

        private void Button_SortedListClear_Click(object sender, EventArgs e)
        {
            listBox_SortedList.Items.Clear();
        }

        private void Button_ClearAll_Click(object sender, EventArgs e)
        {
            listBox_SortedList.Items.Clear();
            listBox_ValueSet.Items.Clear();
        }

        private void NumericUpDown_ByHandValues_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int) Keys.Enter)
            {
                listBox_ValueSet.Items.Add((int)numericUpDown_ByHandValues.Value);
            }
        }
    }
}
